# 项目介绍

> 本项目是一个精简的后台管理页面模板，包含自定义菜单、菜单与标签页的联动，右上角的头像和子菜单定义，对于后台管理项目，可以在此页面的基础上进行二次开发

## 技术栈

- vue3
- vite
- javascript
- pinia
- vue-router

## 使用方式
```shell
cd vue-vite-admin

npm install

npm run dev
```

## 项目截图
- 菜单展开时：
![输入图片说明](public/%E9%A1%B9%E7%9B%AE%E6%88%AA%E5%9B%BE8.png)

- 菜单收缩时：
![输入图片说明](public/%E9%A1%B9%E7%9B%AE%E6%88%AA%E5%9B%BE9.png)