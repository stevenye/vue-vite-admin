import { createRouter, createWebHistory } from 'vue-router'
import Home from '@/views/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: { title: '首页', icon: 'HomeFilled', top: true }
  },
  {
    path: '/info',
    name: 'Info',
    component: () => import('@/views/Info.vue')
  },
  {
    path: '/system-settings',
    name: 'SystemSettings',
    meta: { title: '系统设置', icon: 'Setting',top: true},
    children: [
      {
        path: 'menu-management',
        name: 'MenuManagement',
        component: () => import('@/views/folder-1/MenuManagement.vue'),
        meta: { title: '菜单管理', icon: 'Menu' }
      },
      {
        path: 'role-management',
        name: 'RoleManagement',
        component: () => import('@/views/folder-1/RoleManagement.vue'),
        meta: { title: '角色管理', icon: 'User' }
      },
      {
        path: 'employee-management',
        name: 'EmployeeManagement',
        component: () => import('@/views/folder-1/EmployeeManagement.vue'),
        meta: { title: '权限管理', icon: 'UserFilled' }
      }
    ]
  },
  {
    path: '/user',
    name: 'User',
    meta: { title: '我的空间', icon: 'Monitor', top: true },
    children: [
      {
        path: 'myorder',
        name: 'MyOrder',
        component: () => import('@/views/folder-2/MyOrder.vue'),
        meta: { title: '我的订单', icon: 'List' }
      },
      {
        path: 'photowall',
        name: 'PhotoWall',
        component: () => import('@/views/folder-2/PhotoWall.vue'),
        meta: { title: '我的照片', icon: 'Picture' }
      }
    ]
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router