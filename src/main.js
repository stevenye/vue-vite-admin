import { createApp } from 'vue'
//导入应用
import App from './App.vue'
//导入路由
import router from './router'
//导入pinia
import { createPinia } from 'pinia'
//导入element-plus
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
//导入element-plus图标
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

//创建应用
const app = createApp(App)
//注册图标组件
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
//应用elementplus, router, pinia
app.use(ElementPlus)
app.use(router)
app.use(createPinia())
//挂载应用
app.mount('#app')
